<?php

return [
     [
        'key' => 'catalog.brand',
        'name' => 'brand_lang::app.brand.title',
        'route' => 'brand.index',
        'sort' => 2,
        'icon-class' => '',
    ],
];