<?php

namespace Itec\Brand\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Itec\Brand\Models\Brand::class
    ];
}