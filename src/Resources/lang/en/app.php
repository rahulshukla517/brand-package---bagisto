<?php
return [
    
    'brand' => [
        'title'                 => 'Brand',
        
        'add-type-btn-title'    => 'Add Brand',
        'add-title'             => 'Add Brand',
        'add-image-btn-title'   => 'Add Brand Image',
        'save-btn-title'        => 'Save',
        'edit-title'            => 'Edit Brand',
        'update-btn-title'      => 'Update',


        'general'               => 'General',
        'status_type'           => 'Status & Type',
        'name'                  => 'Name',
        'image'                 => 'Image',
        'description'           => 'Description',
        'slug'                  => 'Slug',
        'status'                => 'Status',
        'status-yes'            => 'Yes',
        'status-no'             => 'No',

    ],


    'datagrid' => [
        'id'                    => 'ID',
        'name'                  => 'Brand Name',
        'image'                 => 'Image',
        'description'           => 'Description',
        'slug'                  => 'Slug',
        'status'                => 'Status',
        'delete'                => 'Do you really want to Delete?',
    ],

    'response' => [
        'create-success'        => 'Brand Added Successfully',
        'create-failed'         => 'Brand Added Failed',
        'update-success'        => 'Brand Update Successfully',
        'update-failed'         => 'Brand Update Failed',
        'delete-success'        => 'Brand Deleted Successfully',
        'delete-failed'         => 'Brand Delete Failed',


        'type-create-success'   => 'Brand Added Successfully',
        'type-create-failed'    => 'Brand Added Failed',
        'type-update-success'   => 'Brand Update Successfully',
        'type-update-failed'    => 'Brand Update Failed',
        'type-delete-success'   => 'Brand Deleted Successfully',
        'type-delete-failed'    => 'Brand Delete Failed',
    ],
    'acl' => [
        'brand-type'         => 'Brand'
    ]
];