<?php

namespace Itec\Brand\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Itec\Brand\Repositories\BrandRepository as Brand;
// use Itec\Brand\Models\BrandTranslation;
use Illuminate\Support\Facades\Event;

/**
 * Catalog Brand controller
 *
 * @author    Jitendra Singh <jitendra@webkul.com>
 * @copyright 2018 Webkul Software Pvt Ltd (http://www.webkul.com)
 */
class BrandController extends Controller
{
    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * BrandRepository object
     *
     * @var array
     */
    protected $brand;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Brand\Repositories\BrandRepository  $brand
     * @return void
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $brands = $this->brand->getBrandTree(null, ['id']);

        return view($this->_config['view'], compact('brands'));
    }

}