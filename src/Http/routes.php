<?php

//Brand Route  

Route::group(['middleware' => ['admin']], function () {

    //Brand Index
    Route::get('admin/catalog/brand', 'Itec\Brand\Http\Controllers\BrandController@index')
        ->defaults('_config', ['view' => 'brand_view::brand.index'])
        ->name('brand.index');
    //Brand Create
    Route::get('admin/catalog/brand/create', 'Itec\Brand\Http\Controllers\BrandController@create')
        ->defaults('_config', ['view' => 'brand_view::brand.create'])
        ->name('brand.create');


    //Brand Store
    Route::post('admin/catalog/brand/store', 'Itec\Brand\Http\Controllers\BrandController@store')
        ->defaults('_config', ['view' => 'brand_view::brand.index'])
        ->name('brand.store');


    //Brand Edit
    Route::get('admin/catalog/brand/edit/{id}', 'Itec\Brand\Http\Controllers\BrandController@edit')
        ->defaults('_config', ['view' => 'brand_view::brand.edit'])
        ->name('brand.edit');


    //Brand Update
     Route::put('admin/catalog/brand/edit/{id}', 'Itec\Brand\Http\Controllers\BrandController@update')
        ->defaults('_config', ['redirect' => 'brand_view::brand.index'])
        ->name('brand.update');


    //Brand Delete
    Route::post('admin/catalog/brand/delete/{id}', 'Itec\Brand\Http\Controllers\BrandController@destroy')->name('brand.delete');

});